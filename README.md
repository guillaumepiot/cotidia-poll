Requirements
============

- Django 1.5, not tested on previous version but is very likely to work.
- jQuery, including AJAX functions


Models
======

Poll
-----

Create the poll item, including question and answers.


Poll submission
---------------------

Record a poll submission containing the answer id and text. Each record belongs to a poll (using a ForeignKey)



Backend
=======

A function to download a CSV file containing the poll result has been written.

The view that create this CSV is in views.py (called: export_answers).


Template tags
=============

In templatetags/poll_tags.py, a function called get_polls renders all the active polls. An argument called limit controls the number of poll returned, by default, it is set to 2.

The form is rendered using the polls.html template.

This template containg the JQuery call to make the AJAX request which submit the result.

* Please note that JQuery must be pre-included in your page template.


AJAX view
=========

The submission is controlled by the submit button, which triggers an AJAX call.

The URL of the call is: /poll/submit/(poll_id)/(answer_id)

This URL will submit the answer and return an HTML view which is re-loaded by JQUERY in to the poll form area.

Bar graph
=========

A simple bar graph is generated from the answer results. It is a pro-rata of the highest value by the width of the box containing the graph.

In models.py, poll model enter the relevant size:

#Enter the div of the width containing the bar graph
div_width = 200

Installation
============  

First of all, you must create a new Django project if you don't have one already and run through the conventional installation procedure (settings, db, etc...), then:

1. Download the django-ajax-poll app
2. Add it to your app list in settings.py:
	INSTALLED_APPS = (
	    'django.contrib.auth',
	    'django.contrib.contenttypes',
	    'django.contrib.sessions',
	    'django.contrib.sites',
	    'django.contrib.messages',
	    'django.contrib.staticfiles',
	 
		'poll'

	)
	
3. Add poll urls.py to the main urls.py:
	urlpatterns = patterns('',
		url(r'^poll/', include('poll.urls'), namespace='poll'),
	) 

4. Include the poll in your template
	{% load poll_tags %}
	<div id="poll">{% show_polls %}</div>  
	
	or
	
	{% load_polls 1 as polls %}
	{% for poll in polls %}
		Render poll...
	{% endfor %}
	
	Note: 1 is the 'limit' of how many polls to load
	
5. Call the javascript init function to initialise the poll

 	load_polls(); 
	

HTML tags
=========

The list of polls need to be implementd as follow:

{% load poll_tags %}
<div id="poll">{% get_polls %}</div>    

Which will render the template polls.html

The AJAX submission will save the result and render the template poll_answer_ajax.html

default.html is an example page rendering the polls all together.
      