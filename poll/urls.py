from django.conf.urls import patterns, include, url

urlpatterns = patterns('poll', 
    url(r'^/?$', 'views.default', name='default'),
    url(r'^submit/(?P<poll_id>[-\d]+)/(?P<answer_id>[-\d]+)/?$', 'views.submit', name='poll-ajax-submit'),
	#poll export data
	url(r'^export-data/(?P<object_id>[-\d]+)?$', 'views.export_answers', name="poll-export-answers"),
)                  