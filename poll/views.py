import csv, codecs, StringIO

from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.template import RequestContext

from poll.models import Poll, PollSubmission

class UnicodeWriter:
	
	"""
	A CSV writer which will write rows to CSV file "f",
	which is encoded in the given encoding.
	"""

	def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
		# Redirect output to a queue
		self.queue = StringIO.StringIO()
		self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
		self.stream = f
		self.encoder = codecs.getincrementalencoder(encoding)()

	def writerow(self, row):
		self.writer.writerow([s.encode("utf-8") for s in row])
		# Fetch UTF-8 output from the queue ...
		data = self.queue.getvalue()
		data = data.decode("utf-8")
		# ... and reencode it into the target encoding
		data = self.encoder.encode(data)
		# write to the target stream
		self.stream.write(data)
		# empty queue
		self.queue.truncate(0)

	def writerows(self, rows):
		for row in rows:
			self.writerow(row)

		
#Decorator to make sure the user is logged in		
@login_required
def export_answers(request, object_id):
	
	#get the poll object
	poll_obj = get_object_or_404(Poll, pk=object_id)
	
	#Create a file name containing the poll id
	filename = 'answer_list_%s.csv' % poll_obj.id
	
	#DEfine the path to the file to create and render
	poll_dir = 'poll'

	
	data = PollSubmission.objects.filter(poll_ref=poll_obj).order_by('submit_date')
	
	csv_export_io = StringIO.StringIO()
	csv_export_io_writer = UnicodeWriter(csv_export_io, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	csv_export_io_writer.writerow([unicode('Poll question'),unicode('Answer id'),unicode('Answer text'),unicode("IP address"),unicode("Submit date")])
	
	for item in data:
		csv_export_io_writer.writerow([unicode(poll_obj.question),unicode(item.answer_id),unicode(item.answer_text),unicode(item.IP_address),unicode(item.submit_date)])
											  
	response = HttpResponse(csv_export_io.getvalue(), mimetype='application/csv')
	response['Content-Disposition'] = 'attachment; filename=%s' % filename

	return response        
	
	
		
def submit(request, poll_id, answer_id):

	
	#get the poll object
	poll_obj = get_object_or_404(Poll, pk=poll_id)
	print poll_obj.get_answers_stat()
	if poll_obj.published() == 1:
		answers = poll_obj.get_answers()
		answer = answers[int(answer_id)-1]
		IP_address = request.META['REMOTE_ADDR']
		PollSubmission.objects.create(poll_ref=poll_obj, answer_id=answer[0], answer_text=answer[1], IP_address=IP_address)
	else:
		poll_obj=False
	#Assign template for AJAX response
	template = 'poll_answer_ajax.html'
	return render_to_response(template, {'poll':poll_obj},
						   context_instance=RequestContext(request))
						
def default(request):

	template = 'default.html'
	return render_to_response(template, {},
						   context_instance=RequestContext(request))
