# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Poll.language_code'
        db.add_column(u'poll_poll', 'language_code',
                      self.gf('django.db.models.fields.CharField')(default='en', max_length=10),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Poll.language_code'
        db.delete_column(u'poll_poll', 'language_code')


    models = {
        u'poll.poll': {
            'Meta': {'object_name': 'Poll'},
            'answers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'publish': ('django.db.models.fields.BooleanField', [], {'max_length': '1'}),
            'publish_date': ('django.db.models.fields.DateField', [], {}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'poll.pollsubmission': {
            'IP_address': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'Meta': {'object_name': 'PollSubmission'},
            'answer_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'answer_text': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['poll.Poll']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['poll']