# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Poll'
        db.create_table(u'poll_poll', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('answers', self.gf('django.db.models.fields.TextField')()),
            ('publish_date', self.gf('django.db.models.fields.DateField')()),
            ('publish', self.gf('django.db.models.fields.BooleanField')(default=False, max_length=1)),
        ))
        db.send_create_signal(u'poll', ['Poll'])

        # Adding model 'PollSubmission'
        db.create_table(u'poll_pollsubmission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poll_ref', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['poll.Poll'])),
            ('answer_id', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('answer_text', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('submit_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'poll', ['PollSubmission'])


    def backwards(self, orm):
        # Deleting model 'Poll'
        db.delete_table(u'poll_poll')

        # Deleting model 'PollSubmission'
        db.delete_table(u'poll_pollsubmission')


    models = {
        u'poll.poll': {
            'Meta': {'object_name': 'Poll'},
            'answers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publish': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '1'}),
            'publish_date': ('django.db.models.fields.DateField', [], {}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'poll.pollsubmission': {
            'Meta': {'object_name': 'PollSubmission'},
            'answer_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'answer_text': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['poll.Poll']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['poll']