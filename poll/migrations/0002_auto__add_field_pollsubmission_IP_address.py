# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PollSubmission.IP_address'
        db.add_column(u'poll_pollsubmission', 'IP_address',
                      self.gf('django.db.models.fields.CharField')(default=0, max_length=250),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PollSubmission.IP_address'
        db.delete_column(u'poll_pollsubmission', 'IP_address')


    models = {
        u'poll.poll': {
            'Meta': {'object_name': 'Poll'},
            'answers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publish': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '1'}),
            'publish_date': ('django.db.models.fields.DateField', [], {}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'poll.pollsubmission': {
            'IP_address': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'Meta': {'object_name': 'PollSubmission'},
            'answer_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'answer_text': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['poll.Poll']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['poll']