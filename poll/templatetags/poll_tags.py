import datetime

from django.template import Library, Node, resolve_variable, TemplateSyntaxError     
from django.template import RequestContext
from django.utils import translation
from django.utils.timezone import now
from django.conf import settings

register = Library()

from poll.models import Poll

@register.inclusion_tag("polls.html")
def show_polls(limit=2):
    date = now()
    today = datetime.date(year=date.year, month=date.month, day=date.day) 
    if len(settings.LANGUAGES) > 1:
        polls = Poll.objects.filter(publish=1,  publish_date__lte=today, language_code=translation.get_language()).order_by('-publish_date')[:limit]
    else:
        polls = Poll.objects.filter(publish=1,  publish_date__lte=today).order_by('-publish_date')[:limit]
    return {"polls":polls}

def load_polls(context, limit):
    date = now()
    today = datetime.date(year=date.year, month=date.month, day=date.day) 
    if len(settings.LANGUAGES) > 1:
        polls = Poll.objects.filter(publish=1,  publish_date__lte=today, language_code=translation.get_language()).order_by('-publish_date')[:limit]
    else:
        polls = Poll.objects.filter(publish=1,  publish_date__lte=today).order_by('-publish_date')[:limit]
    return polls

register.assignment_tag(takes_context=True)(load_polls)