from poll.models import Poll, PollSubmission
from django.contrib import admin 
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _  
from django.conf import settings          

class PollAdmin(admin.ModelAdmin):
    actions = ['delete_selected']
    list_filter = ['publish']
    list_display = ['question', 'list_answers', 'publish', 'publish_date', 'download_answers']
    
    fieldsets = (
        ('Content', {
            'fields': ('question', 'answers', 'publish_date', 'publish',)
        }),
        
    )

    if len(settings.LANGUAGES) > 1:
        fieldsets = (
            ('Content', {
                'fields': ('language_code', 'question', 'answers', 'publish_date', 'publish',)
            }),
            
        )
    
    
    def list_answers(self, obj):
        answers = obj.answers.splitlines(True)
        a_list = ""
        for a in answers:
            a_list += '%s<br />' % a.strip()
        return a_list
    list_answers.short_description = 'Answers' 
    list_answers.allow_tags = True
    
    def download_answers(self, obj):
        
        return '<a href="%s">%s</a>' % (reverse('poll:poll-export-answers',kwargs={'object_id':obj.id}), 'Download CSV')
    download_answers.short_description = 'Data dump' 
    download_answers.allow_tags = True

    def language(self, obj):
        for language in settings.LANGUAGES:
            if language[0] == obj.language_code:
                return u'<img src="/static/admin/img/flags/%s.png" alt="" rel="tooltip" data-title="%s">' % (language[0], language[1])
        return ''
    language.allow_tags = True

    def get_list_display(self, request, obj=None):
        if not len(settings.LANGUAGES) > 1:
            return ['question', 'list_answers', 'publish', 'publish_date', 'download_answers']
        else:
            return ['question', 'list_answers', 'publish', 'publish_date', 'language', 'download_answers']


class PollSubmissionAdmin(admin.ModelAdmin):
    list_filter = ['poll_ref']
    list_display = ['poll_ref', 'answer_id', 'answer_text', 'IP_address', 'submit_date']
    actions = ['delete_selected']
    
    
admin.site.register(Poll, PollAdmin) 
admin.site.register(PollSubmission, PollSubmissionAdmin) 

   